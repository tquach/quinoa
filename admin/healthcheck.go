package admin

import (
	"github.com/go-martini/martini"
	"labix.org/v2/mgo"
)

const (
	PathHealthCheck = "/healthcheck"
)

type HealthCheck martini.Handler

func DatabaseHealthCheck() HealthCheck {
	return func(mgo *mgo.Database) string {
		err := mgo.Session.Ping()
		if err != nil {
			return "OK"
		} else {
			return err.Error()
		}
	}
}

func BasicCheck() HealthCheck {
	return func() string {
		return "OK"
	}
}
