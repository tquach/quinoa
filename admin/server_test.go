package admin

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-martini/martini"
	. "launchpad.net/gocheck"
)

func TestServer(t *testing.T) { TestingT(t) }

type ServerTestSuite struct{}

var _ = Suite(&ServerTestSuite{})

func (suite *ServerTestSuite) TestBasicHealthCheck(c *C) {
	recorder := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/healthcheck", nil)

	s := NewAdminServer([]martini.Handler{})
	s.Martini.ServeHTTP(recorder, req)

	c.Assert(recorder.Code, Equals, http.StatusOK)
}

func (suite *ServerTestSuite) TestShouldPingDatabase(c *C) {

}
