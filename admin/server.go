package admin

import (
	"net/http"
	"os"

	"github.com/go-martini/martini"
)

var (
	hostname string
)

// Martini represents the top level web application. inject.Injector methods can be invoked to map services on a global level.
type AdminServer struct {
	*martini.Martini
	healthChecks []HealthCheck
}

func NewAdminServer(handlers []martini.Handler) *AdminServer {
	m := martini.New()
	r := martini.NewRouter()

	// Add handlers
	m.Handlers(handlers)
	m.Action(r.Handle)

	// Add health checks, including basic check
	adm := AdminServer{Martini: m}
	adm.Check(BasicCheck())
	adm.Use(HealthCheckHandler())
	return &adm
}

// HealthChecks sets the entire middleware stack with the given HealthChecks. This will clear any current middleware handlers.
// Will panic if any of the handlers is not a callable function
func (adm *AdminServer) HealthChecks(healthChecks ...HealthCheck) {
	adm.healthChecks = make([]HealthCheck, 0)
	for _, h := range healthChecks {
		adm.Check(h)
	}
}

// Set a custom health check handler
func (adm *AdminServer) Check(healthCheck HealthCheck) {
	adm.Check(healthCheck)
}

func HealthCheckHandler() martini.Handler {
	return func(c *martini.Context, res http.ResponseWriter, req *http.Request) {
	}
}

func init() {
	hostname, _ = os.Hostname()
}
